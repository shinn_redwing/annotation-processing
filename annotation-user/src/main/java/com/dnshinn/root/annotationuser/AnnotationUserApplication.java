package com.dnshinn.root.annotationuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnotationUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnnotationUserApplication.class, args);
    }

}
